#!/usr/bin/env python3

from loadleveller import jobstatus, jobfile
import argparse
import subprocess
import json
import os
import shutil

parser = argparse.ArgumentParser(description='Concatenate job results and merge them if needed')
parser.add_argument('jobfiles', nargs='+', help='jobs to concatenate')
parser.add_argument('-o', '--output', required=True, help='output file for the concatenated result.')
parser.add_argument('-n', '--no-merge', action='store_true', help='skip the merging step')
parser.add_argument('-c', '--copy-jobfile', action='store_true', help='copy <jobfiles> to the output in a .scripts directory.')
args = parser.parse_args()

os.makedirs(os.path.dirname(os.path.abspath(args.output)), exist_ok=True)
if args.copy_jobfile:
    job_archive_dir = os.path.splitext(args.output)[0]+'.scripts'
    os.makedirs(job_archive_dir, exist_ok=True)

used_short_jobnames = set()

tasks = []
for jobname in args.jobfiles:
    try:
        j = jobfile.JobFile(jobname)
        if not args.no_merge:
            if jobstatus.job_need_merge(j):
                subprocess.run(['loadl', 'm', jobname])

        with open(j.jobname+'.results.json', 'r') as infile:
            res = json.load(infile)
            tasks += res

        if args.copy_jobfile:
            short_jobname = os.path.basename(jobname)
            if short_jobname in used_short_jobnames:
                idx = 2
                while "{}_{}".format(short_jobname, idx) in used_short_jobnames:
                    idx += 1
                short_jobname = "{}_{}".format(short_jobname, idx)
            used_short_jobnames.add(short_jobname)

            shutil.copyfile(jobname, os.path.join(job_archive_dir,short_jobname))
    except FileNotFoundError as e:
        print('File "{}" not found for "{}". Skipping...'.format(e.filename, jobname))
with open(args.output, 'w') as outfile:
    json.dump(tasks, outfile, indent=0)

print('{} -> {}'.format(' '.join(args.jobfiles), args.output))
            
